# Generated by Django 2.1.7 on 2020-02-07 11:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Scrum', '0016_remove_chatmessage_project'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScrumBoardConnectionId',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('connection_id', models.CharField(max_length=255)),
            ],
        ),
    ]
