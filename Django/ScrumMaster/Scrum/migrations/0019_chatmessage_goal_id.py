# Generated by Django 2.1.7 on 2020-03-04 12:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Scrum', '0018_chatconnectionid_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatmessage',
            name='goal_id',
            field=models.CharField(default=-1, max_length=100),
            preserve_default=False,
        ),
    ]
